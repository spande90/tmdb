//
//  MoviesData.swift
//  TMDB
//
//  Created by Siddharth on 02/07/20.
//  Copyright © 2020 Siddharth. All rights reserved.
//

import Foundation

var totalPage:Int = 0

class MoviesData{
    
    
    var id:Int?
    var adult:Bool?
    var backdrop_path:String?
    var genre_id:[String:Any]?
    var media_type:String?
    var original_language:String?
    var original_title:String?
    var overview:String?
    var popularity:Double?
    var poster_path:String?
    var release_date:String?
    var title:String?
    var video:Bool?
    var vote_average: Double
    var vote_count: Double
    
    init(id:Int?,adult:Bool?,backdrop_path:String?,genre_id:[String:Any]?,media_type:String?,original_language:String?,original_title:String?,overview:String?,popularity:Double?,poster_path:String?,release_date:String?,title:String?,
         video:Bool?,
         vote_average: Double?,vote_count: Double?)
    {
        self.id = id
        self.adult = adult
        self.backdrop_path = backdrop_path
        self.genre_id = genre_id
        self.media_type = media_type
        self.original_language = original_language
        self.original_title = original_title
        self.overview = overview
        self.popularity = popularity
        self.poster_path = poster_path
        self.release_date = release_date
        self.title = title
        self.video = video
        self.vote_average = vote_average!
        self.vote_count = vote_count!
    }
    
    init() {
        self.id = 0
        self.adult = false
        self.backdrop_path = ""
        self.genre_id = [:]
        self.media_type = ""
        self.original_language = ""
        self.original_title = ""
        self.overview = ""
        self.popularity = 0
        self.poster_path = ""
        self.release_date = ""
        self.title = ""
        self.video = false
        self.vote_average = 0
        self.vote_count = 0
    }
    
    class func getMoviesListThisWeek(page:Int, completion: @escaping (_ message: String?, _ data : [MoviesData]) -> Void)
    {
        let param = ["api_key":ApiToken,"page":page.description]
        var moviesList:[MoviesData] = [MoviesData]()
        RESTManager.sharedInstance.callWebService(toURL:GetTrendingWeekRequest, withHttpMethod: .get, param: param, onSuccess: { (response) in
            if Array(response.keys).contains("total_pages"){
                totalPage = response["total_pages"] as! Int
            }
            if Array(response.keys).contains("results")
            {
                let ar = response["results"] as! [[String : Any]]
                for i in 0..<ar.count
                {
                    let dic = ar[i]
                    let movieObj = MoviesData()
                    movieObj.id = dic["id"] as? Int
                    movieObj.adult = (dic["adult"] as! Bool)
                    movieObj.backdrop_path = dic["backdrop_path"] as? String
                    movieObj.genre_id = dic["genre_id"] as? [String:Any]
                    movieObj.media_type = dic["media_type"] as? String
                    movieObj.original_title = dic["original_title"] as? String
                    movieObj.release_date = dic["release_date"] as? String
                    movieObj.original_language = dic["original_language"] as? String
                    movieObj.overview = dic["overview"] as? String
                    movieObj.popularity = dic["popularity"] as? Double
                    movieObj.poster_path = dic["poster_path"] as? String
                    movieObj.title = dic["title"] as? String
                    movieObj.video = (dic["video"] as! Bool)
                    movieObj.vote_average = (dic["vote_average"] as! Double)
                    movieObj.vote_count = (dic["vote_count"] as! Double)
                    moviesList.append(movieObj)
                }
            }
            completion(ResponseMsg.KSuccess,moviesList)
        }) { (error) in
            completion(error.localizedDescription,moviesList)
        }
    }
}
