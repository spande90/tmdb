//
//  Constant.swift
//  TMDB
//
//  Created by Siddharth on 30/06/20.
//  Copyright © 2020 Siddharth. All rights reserved.
//

import Foundation
import UIKit

//Screen Constanst
struct Screen{
    static let width = UIScreen.main.bounds.width
    static let height = UIScreen.main.bounds.height
}

//URL Constants
let BaseUrl:String = "https://api.themoviedb.org/3/"
let GetTrendingWeekRequest:String = "trending/movie/week"
let ApiToken:String = "8eac22f4c24d01c480e4d99fef2edfc3"

//Image Downlaod Quantity Constants
struct ImageQualityURL{
    static let ImageBaseUrlw500:String = "https://image.tmdb.org/t/p/w500"
    static let ImageBaseUrlw200:String = "https://image.tmdb.org/t/p/w200"
    static let ImageBaseUrlOriginal:String = "https://image.tmdb.org/t/p/original"
}

//VC Name Constants
struct VC{
    static let kMovieListVC:String = "MovieListVC"
    static let kMovieDetailVC:String = "MovieDetailVC"
}

//Cell Name Constants
struct  Cell {
    static let kMovieListCell:String = "MovieCell"
}

//Response Message Constants
struct ResponseMsg{
    static let KSuccess:String = "Success"
    static let kFail:String = "Fail"
}

//Image PlaceHolders Constants
struct Placeholder{
    static let posterPlaceHolderImage:String = "ic_placeholder_poster"
}
