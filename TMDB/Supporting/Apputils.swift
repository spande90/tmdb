//
//  Apputils.swift
//  TMDB
//
//  Created by Siddharth on 03/07/20.
//  Copyright © 2020 Siddharth. All rights reserved.
//

import Foundation

class Apputils {
    //Get date from String
    class func getDateFromString(dateInString:String,format:String)->Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: dateInString)
        return date!
    }
    //Get String from date
    class func getStringFromDate(date: Date, dateformat format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format // Your New Date format as per requirement change it own
        
        let newDate: String = dateFormatter.string(from: date) // pass Date here
        // New formatted Date string
        return newDate
    }
    class func showMessagePrompt(_ message:String){
        TinyToast.shared.show(message: message, valign: .bottom, duration: .short)

    }
}
