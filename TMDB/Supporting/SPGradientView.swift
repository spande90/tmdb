//
//  SPGradientView.swift
//  TMDB
//
//  Created by Siddharth on 03/07/20.
//  Copyright © 2020 Siddharth. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable class SPGradientView: UIView {
    //Choosing the first Color in the Gradient
    @IBInspectable var firstColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    //Choosing the second Color in the Gradient
    @IBInspectable var secondColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    //Setting the rendering position
    @IBInspectable var isHorizontal: Bool = true {
        didSet {
            updateView()
        }
    }
    // overriding the layer class of view's frame
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    // Updating the view frame with color
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor, secondColor].map {$0.cgColor}
        if (isHorizontal) {
            layer.startPoint = CGPoint(x: 0, y: 0.5)
            layer.endPoint = CGPoint (x: 1, y: 0.5)
        } else {
            layer.startPoint = CGPoint(x: 0.5, y: 0)
            layer.endPoint = CGPoint (x: 0.5, y: 1)
        }
    }
    
}
