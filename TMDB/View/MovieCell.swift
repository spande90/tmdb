//
//  MovieCell.swift
//  TMDB
//
//  Created by Siddharth on 02/07/20.
//  Copyright © 2020 Siddharth. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
    
    @IBOutlet weak var lblRating:UILabel!
    @IBOutlet weak var imgPoster:UIImageView!
    @IBOutlet weak var lblTitleName:UILabel!
    @IBOutlet weak var lblLanguage:UILabel!
    private var task: URLSessionDataTask?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        // dismiss previos handle of the cell in reuse
        task?.cancel()
        task = nil
        imgPoster.image = nil
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgPoster.image = UIImage(named: Placeholder.posterPlaceHolderImage)
    }
    //configuring the cell
    func configureCell(movieData:MoviesData){
        self.lblRating.text = movieData.vote_average.description
        let cellImageUrl:String = ImageQualityURL.ImageBaseUrlw200 + movieData.poster_path!
        self.lblTitleName.text = movieData.original_title
        if task == nil {
            // Ignore calls when reloading
            task = self.imgPoster.downloadImage(from: cellImageUrl)
        }
        
    }

}
