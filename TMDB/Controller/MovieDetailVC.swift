//
//  MovieDetailVC.swift
//  TMDB
//
//  Created by Siddharth on 03/07/20.
//  Copyright © 2020 Siddharth. All rights reserved.
//

import UIKit

class MovieDetailVC: UIViewController {

    //Varible Decleration
    var movieDetails:MoviesData!
    
    //Outlet Decleration
    @IBOutlet weak var imgMoviePoster:UIImageView!
    @IBOutlet weak var imgMovieBackDrop:UIImageView!
    @IBOutlet weak var imgAdult:UIImageView!
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblOriginalTitle:UILabel!
    @IBOutlet weak var lblRatings:UILabel!
    @IBOutlet weak var lblPopularity:UILabel!
    @IBOutlet weak var lblReleaseDate:UILabel!
    @IBOutlet weak var lblLanguage:UILabel!
    @IBOutlet weak var lblVotes:UILabel!
    @IBOutlet weak var txtDescription:UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeVC()
        
    }
    
   
}
extension MovieDetailVC{
    func initializeVC()
    {
        
        if movieDetails != nil{
            
            imgAdult.isHidden =  movieDetails.adult == false ? true :false
            let posterPath = ImageQualityURL.ImageBaseUrlw500 + movieDetails.poster_path!
            self.imgMoviePoster.downloadImage(from: posterPath)
            let backdropPath = ImageQualityURL.ImageBaseUrlw500 + movieDetails.backdrop_path!
            self.imgMovieBackDrop.downloadImage(from: backdropPath)
            self.lblTitle.text = movieDetails.title
            self.txtDescription.text = movieDetails.overview!
            self.lblRatings.text = movieDetails.vote_average.description
            self.lblPopularity.text = movieDetails.popularity?.description
            self.lblLanguage.text = movieDetails.original_language!.uppercased()
            let movieDate = Apputils.getStringFromDate(date: Apputils.getDateFromString(dateInString: movieDetails.release_date!, format: "yyyy-mm-dd"), dateformat: "dd MMM yyyy")
            self.lblReleaseDate.text = movieDate
            self.lblVotes.text = movieDetails.vote_count.description
        }
        
    }
}
