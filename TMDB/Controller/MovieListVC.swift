//
//  MovieListVC.swift
//  TMDB
//
//  Created by Siddharth on 02/07/20.
//  Copyright © 2020 Siddharth. All rights reserved.
//

import UIKit

class MovieListVC: UIViewController {
    //Outlets Decleration
    @IBOutlet weak var movieListColl:UICollectionView!
    
    //Variable Decleration
    fileprivate var pageCount:Int = 1
    fileprivate var movieList:[MoviesData] = []
    
    //Loading the view
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeVC()
    }
    
}

extension MovieListVC{
    //Initializing the Controller with routine tasks.
    fileprivate func initializeVC(){
        registerCells()
        self.movieListColl.delegate = self
        self.movieListColl.dataSource = self
        if NetworkManager.isReachable(){
            callMovieListWebServices()
        }else{
            DispatchQueue.main.async {
                Apputils.showMessagePrompt("No Network Found!")
            }
            
        }
        
    }
    //Decleration of Nib with the cell class to the collection View
    fileprivate func registerCells(){
        self.movieListColl.register(UINib(nibName: Cell.kMovieListCell, bundle: nil), forCellWithReuseIdentifier: Cell.kMovieListCell)
    }
    //Request to the model class for data fetching with completion
    fileprivate func callMovieListWebServices(){
        
        MoviesData.getMoviesListThisWeek(page:pageCount) { (Message, movieList) in
            if Message == ResponseMsg.KSuccess{
                for movie in movieList{
                    self.movieList.append(movie)
                }
                DispatchQueue.main.async{
                    self.movieListColl.reloadData()
                }
            }else{
                DispatchQueue.main.async {
                    Apputils.showMessagePrompt(Message!)
                }
                //print(Message!)
            }
        }
    }
    
}
//collection view methods to handle
extension MovieListVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    //number of movie object in the list
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieList.count
    }
    // configuring the movie cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let movieCell = collectionView.dequeueReusableCell(withReuseIdentifier:Cell.kMovieListCell, for: indexPath) as! MovieCell
        movieCell.configureCell(movieData: movieList[indexPath.row])
        return movieCell
    }
    // sizing of movie cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: Screen.width / 3 , height: Screen.height / 3)
    }
    // selection of particular movie cell
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: VC.kMovieDetailVC) as! MovieDetailVC
        vc.movieDetails = movieList[indexPath.row]
        self.present(vc, animated: true, completion: nil)
    }
}
extension MovieListVC{
    
    // pagination request call using the property of collection view
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool){
        if scrollView == movieListColl
        {
            if ((self.movieListColl.contentOffset.y + self.movieListColl.frame.size.height) >= self.movieListColl.contentSize.height)
            {
                // if the height increases call pagination
                if NetworkManager.isReachable(){
                    //increase the count of page only if network is reachable and call web service
                    pageCount += 1
                    callMovieListWebServices()
                }else{
                    Apputils.showMessagePrompt("No Network Found!")
                }
            }
        }
    }
}
